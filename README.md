# Projeto Integrador - Grupo 05 PI Banco de Dados

# Objetivo:
Desenvolver um aplicativo que possa auxiliar na melhoria do serviço e comunicação entre motorista e usuários de uma van com destino prévio.

# Requisitos:

- Gerenciamento de destino: Cada destino possui descrição, local, horário de chegada e horário de volta. Disponível apenas para o motorista.​​

- Gerenciamento de passageiros: Cada passageiro tem nome, telefone de contato, endereço e um ou mais destinos. Disponível apenas para o motorista.​​

- Definição de rota: A partir de um ponto de saída e um destino, o app sugere a ordem em que os passageiros devem ser apanhados. O motorista pode definir, de acordo com sua experiência e condições de trânsito o horário estimado em que chegará na localidade de cada passageiro. ​​

- Função "hora prevista": O app deve informar ao passageiro a hora prevista de chegada da van (na ida) e qual o horário previsto para entrega (na volta).

Utilizar APP INVENTOR

# Planejamento:

- SPRINT 0 - Cadastro do motorista e do passageiro com implementação do Banco de Dados; Tela de Login;

- SPRINT 1 - Cadastro do Destino; Cadastro de Viagem; Visualização de viagens pelo motorista e passageiro;  

- SPRINT 2 - Cálculo simples da análise de rota; Tela de Início de viagem;

- SPRINT 3 - Adição de atrasos à viagens; Aprimorar o cálculo de rotas;

# Issues entregues:

- SPRINT 0: <https://gitlab.com/pi_grupo5/projeto_integrador/-/milestones/1> 

- SPRINT 1: <https://gitlab.com/pi_grupo5/projeto_integrador/-/milestones/2>

# Tecnologias Utilizadas:

- MIT APP INVENTOR
- GitLab para gerenciamento do projeto

# Membros do grupo:

- Kainé Santanna

- Nágella Nasser

- Danielly Garcia - Master

- Marcelo Fernando - Master

# Videos de Apresentação:

- SPRINT 0: <https://www.youtube.com/watch?v=dgjxCT9QQsw>

- SPRINT 1: <https://youtu.be/ympt3z_2n_s>

# Backlog:

<https://gitlab.com/pi_grupo5/projeto_integrador/-/issues>






